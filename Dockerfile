# pull official base image
FROM python:3.9.10-slim-bullseye

# set work directory
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

# install dependencies
RUN python -m pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r /app/requirements.txt

# copy project
COPY . .

# collect static files
RUN python /app/mini-project/manage.py collectstatic --noinput

# add and run as non-root user
RUN adduser myuser
USER myuser

# run gunicorn
CMD gunicorn --chdir /app/mini-project mysite.wsgi:application --bind 0.0.0.0:$PORT
