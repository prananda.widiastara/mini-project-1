<div align="center">
<p>
  <a href="https://gitlab.com/prananda.widiastara/mini-project-1">
    <img src="./images/logo.jpeg" alt="Logo">
  </a>

  <h3 align="center">Mini Project 1</h3>

  <p align="center">
   Back-end Engineer Intern
    <br />
    <a href="https://gitlab.com/prananda.widiastara/mini-project-1"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/prananda.widiastara/mini-project-1/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/prananda.widiastara/mini-project-1/-/issues">Request Feature</a>
  </p>
</p>
</div>


<!-- TABLE OF CONTENTS -->
## Table of Contents

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
- [Requirements](#requirements)
- [Local development](#local-development)
  - [Installing required dependency](#installing-required-dependency)
  - [Configure App](#configure-app)
  - [Running the app](#running-the-app)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Maintainers](#maintainers)
- [Acknowledgements](#acknowledgements)
- [License](#license)

<!-- ABOUT THE PROJECT -->
## About The Project

Mini Project 1 is a Task for MSIB Intern to build a Message Board Application along with team consisting of Back-end Engineer and Front-end Engineer

### Built With

* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Django Rest Framework](https://www.django-rest-framework.org/)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

## Requirements
* [Pyenv](https://github.com/pyenv/pyenv) (**Recommended**) for python version management
* Python 3.9.10
  * To install using pyenv
    ```
    pyenv install 3.9.10
    ```
* [Poetry](https://python-poetry.org/) for Python package and environment management.
* [Git Clone](https://git-scm.com/docs/git-clone) this repository

## Local development
### Installing required dependency
1. Clone the repository
    ```sh
    $ git clone https://gitlab.com/prananda.widiastara/mini-project-1
    ```
1. Install each dependency from the requirements section above.
2. Install python dependencies
    ```
    poetry install
    ```
4. Install pre-commit git hook (for auto formatting purpose)
    ```
    pre-commit install
    ```

### Configure App
1. Find this file
   - [`./config/default.env`](./config/default.env)
2. Duplicate that file and rename it to `.env`
3. Open newly created `.env` file and adjust the content

### Running the App

1. Run poetry shell to run the virtual environment
    ```
    poetry shell
    ```
2. Run database migrations script
    ```
    python manage.py migrate
    ```
3. Run django web dev server
    ```
    python manage.py runserver
    ```

## Testing

### Setup

- Install Postman or other API testing tools

- Import the [OpenAPI schema files](api.json) to your API testing tools.

### API Testing

- Test all the available endpoints and HTTP Requests from the imported files

<!-- USAGE EXAMPLES -->
## Usage

Additional Usages

* Add Dockerfile script to build your application into docker image
* Please don't hardcode the configuration
* Try to implement clean code best practices
* Don't forget to add technical documentation and configuration details **(you're not immortal)**

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/prananda.widiastara/mini-project-1/-/issues) for a list of proposed features (and known issues).

<!-- MAINTAINERS -->
## Maintainer
* [Prananda Nur Widiastara](mailto:prananda.widiastara@prosa.ai)

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
 List of library used

* [Markdown](https://www.markdownguide.org/)
* [Dockerfile](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

## License
Copyright (c) 2022, [Prosa.ai](https://prosa.ai).
