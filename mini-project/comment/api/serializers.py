from rest_framework import serializers
from comment.models import Comment
from mysite.serializers import AccountRelationSerializer, PostRelationSerializer


class CommentSerializer(serializers.ModelSerializer):
    post = PostRelationSerializer(read_only=True)
    owner = AccountRelationSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"
