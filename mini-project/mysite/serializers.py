from rest_framework import serializers
from post.models import Post
from comment.models import Comment
from account.models import Account


class AccountRelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ["id", "username", "name", "profile_pic"]


class PostRelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ["id", "title", "body", "created_at", "updated_at"]


class CommentRelationSerializer(serializers.ModelSerializer):
    owner = AccountRelationSerializer(read_only=True)

    class Meta:
        model = Comment
        exclude = ["post"]
