import os
from mysite.settings.components import config, BASE_DIR

SECRET_KEY = config("SECRET_KEY", default=os.environ.get("SECRET_KEY"))

DEBUG = False

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    # my app
    "account",
    "comment",
    "post",
    # rest framework
    "rest_framework",
    "rest_framework.authtoken",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "drf_spectacular",
    # cleanup filefield
    "django_cleanup.apps.CleanupConfig",
    # cloudinary
    "cloudinary_storage",
    "cloudinary",
    # cors
    "corsheaders",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "mysite.urls"

WSGI_APPLICATION = "mysite.wsgi.application"

# Authentication

AUTH_USER_MODEL = "account.Account"

TOKEN_EXPIRED_AFTER_SECONDS = config(
    "TOKEN_EXPIRED_AFTER_SECONDS", default=3600, cast=int
)

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Database

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

# File settings

MEGABYTE_LIMIT = 102400

MEDIA_URL = "/mini-project-1/"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

TEMP = os.path.join(BASE_DIR, "temp")

# Globalization settings

LANGUAGE_CODE = "en-us"

TIME_ZONE = "Asia/Jakarta"

USE_I18N = True

USE_TZ = False

# Static

STATIC_URL = "/static/"

STATIC_ROOT = BASE_DIR / "staticfiles"

STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# Template engine settings

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# Cloudinary media storage

CLOUDINARY_STORAGE = {
    "CLOUD_NAME": config("CLOUD_NAME"),
    "API_KEY": config("API_KEY"),
    "API_SECRET": config("API_SECRET"),
    "STATICFILES_MANIFEST_ROOT": os.path.join(BASE_DIR, "my-manifest-directory"),
}

DEFAULT_FILE_STORAGE = "cloudinary_storage.storage.MediaCloudinaryStorage"

# CSRF

CSRF_TRUSTED_ORIGINS = ["https://prosaintern-mp1.herokuapp.com"]

# CORS HEADERS

CORS_ALLOW_ALL_ORIGINS = True

# Default auto field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
