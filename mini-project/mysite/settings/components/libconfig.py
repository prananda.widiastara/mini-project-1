from mysite.settings.components import config

PAGINATION_PAGE_SIZE = config("PAGINATION_PAGE_SIZE", default=10, cast=int)

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "account.authentication.ExpiringTokenAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
    ],
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": PAGINATION_PAGE_SIZE,
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Mini Project 1 API",
    "DESCRIPTION": "Mini project 1 backend engineer internship prosa",
    "VERSION": "0.1.0",
    "SERVE_INCLUDE_SCHEMA": False,
}
