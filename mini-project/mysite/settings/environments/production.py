import os
import dj_database_url
from mysite.settings.components import config, BASE_DIR

DEBUG = False

ALLOWED_HOSTS = [
    "localhost",
    "127.0.0.1",
    "prosaintern-mp1.herokuapp.com",
    "127.0.0.1:3000",
    "localhost:3000",
]


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

DATABASE_URL = config("DATABASE_URL", default=os.environ.get("DATABASE_URL"))

db_from_env = dj_database_url.config(
    default=DATABASE_URL, conn_max_age=500, ssl_require=True
)
DATABASES["default"].update(db_from_env)
