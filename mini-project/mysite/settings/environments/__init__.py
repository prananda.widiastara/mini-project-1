from decouple import AutoConfig
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent.parent
config = AutoConfig(search_path=BASE_DIR.joinpath("config"))

from dotenv import load_dotenv

load_dotenv(BASE_DIR.joinpath("config/.env"))
