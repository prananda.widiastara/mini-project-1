from pathlib import Path

from decouple import AutoConfig
from split_settings.tools import optional, include

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent
config = AutoConfig(search_path=BASE_DIR.joinpath("config"))
ENV = config("DJANGO_ENV")

base_settings = [
    "components/*.py",
    # Select the right env:
    "environments/{0}.py".format(ENV),
    # Optionally override some settings:
    # optional("environments/local.py"),
]

# Include settings:
include(*base_settings)
