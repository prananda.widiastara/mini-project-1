from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class MyAccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            return ValueError("Users must have an email")
        if not username:
            return ValueError("Users must have an username")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            username=username,
            password=password,
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


# upload file path
def upload_location(instance, filename):
    file_path = "profile_pic/{user_id}/{user_username}-{filename}".format(
        user_id=str(instance.id),
        user_username=str(instance.username),
        filename=filename,
    )
    return file_path


class LowerCaseUsernameField(models.CharField):
    def __init__(self, *args, **kwargs):
        super(LowerCaseUsernameField, self).__init__(*args, **kwargs)

    def get_prep_value(self, value):
        return str(value).lower()


class LowerCaseEmailField(models.EmailField):
    def to_python(self, value):
        value = super(LowerCaseEmailField, self).to_python(value)
        if isinstance(value, str):
            return value.lower()
        return value


class Account(AbstractBaseUser):
    email = LowerCaseEmailField(
        verbose_name="email", max_length=60, unique=True, null=False, blank=False
    )
    username = LowerCaseUsernameField(max_length=30, unique=True)
    name = models.CharField(max_length=50, null=False, blank=False)
    profile_pic = models.ImageField(upload_to=upload_location, null=True, blank=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True)
    created_at = models.DateTimeField(verbose_name="date created", auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="date updated", auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = [
        "username",
    ]

    objects = MyAccountManager()

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
