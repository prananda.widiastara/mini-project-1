from rest_framework import serializers
from django.core.files.images import get_image_dimensions
from django.conf import settings


def image_validator(image):
    filesize = image.size
    width, height = get_image_dimensions(image)

    if width / height != 1:
        raise serializers.ValidationError(
            f"You need to upload an image with 1:1 dimensions"
        )

    if filesize > settings.MEGABYTE_LIMIT:
        raise serializers.ValidationError(
            f"Max file size is {settings.MEGABYTE_LIMIT}KB"
        )
