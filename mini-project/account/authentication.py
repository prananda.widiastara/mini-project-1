from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed

from datetime import timedelta
from django.utils import timezone, dateformat
from django.conf import settings


def expires_in(token):
    expires_in = token.created + timedelta(seconds=settings.TOKEN_EXPIRED_AFTER_SECONDS)
    return expires_in


def formated_expires_in(token):
    return dateformat.format(expires_in(token), "Y-m-d H:i:s")


def is_token_expired(token):
    return expires_in(token) < timezone.now()


class ExpiringTokenAuthentication(TokenAuthentication):
    """
    If token is expired then it will be removed
    """

    def authenticate_credentials(self, key):
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise AuthenticationFailed("Invalid Token")

        if not token.user.is_active:
            raise AuthenticationFailed("User is not active")

        is_expired = is_token_expired(token)
        if is_expired:
            token.delete()
            raise AuthenticationFailed(
                "The Token is expired, login to refresh the token"
            )

        return (token.user, token)
