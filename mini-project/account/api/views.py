from rest_framework.authtoken.models import Token
from account.models import Account
from account.api.serializers import (
    AccountSerializer,
    RegistrationSerializer,
    LoginSerializer,
    ProfileSerializer,
    ProfileUpdateSerializer,
)

from account.authentication import ExpiringTokenAuthentication
from account.permission import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from account.renderers import MyRenderer
from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination


class UserListRegistration(generics.ListCreateAPIView):
    authentication_classes = []
    permission_classes = []
    pagination_class = PageNumberPagination
    renderer_classes = [MyRenderer]
    queryset = Account.objects.all().order_by("id")

    def get_serializer_class(self):
        if self.request.method == "POST":
            return RegistrationSerializer
        return AccountSerializer


class UserLogin(generics.GenericAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = LoginSerializer
    renderer_classes = [MyRenderer]

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.validated_data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLogout(generics.GenericAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = AccountSerializer
    renderer_classes = [MyRenderer]

    def post(self, request):
        account = request.user
        try:
            token = Token.objects.get(user=account)
        except Token.DoesNotExist:
            return Response(
                {"detail": "You can't perform this action"},
                status=status.HTTP_403_FORBIDDEN,
            )

        token.delete()
        return Response({"detail": "Logout successful"}, status=status.HTTP_200_OK)


class UserProfile(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    renderer_classes = [MyRenderer]
    queryset = Account.objects.all()
    lookup_field = "id"

    def get_serializer_class(self):
        if self.request.method in ("PUT", "PATCH"):
            return ProfileUpdateSerializer
        return ProfileSerializer
