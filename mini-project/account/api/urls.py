from django.urls import path

from . import views

app_name = "account"
urlpatterns = [
    path("", views.UserListRegistration.as_view(), name="user-account"),
    path("<int:id>/", views.UserProfile.as_view(), name="user-profile"),
    path("login/", views.UserLogin.as_view(), name="user-login"),
    path("logout/", views.UserLogout.as_view(), name="user-logout"),
]
