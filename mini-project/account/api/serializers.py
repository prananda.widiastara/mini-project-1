from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

from django.contrib.auth import authenticate
from account.authentication import is_token_expired, formated_expires_in
from account.models import Account
from rest_framework.authtoken.models import Token

from account.utils import image_validator
from mysite.serializers import PostRelationSerializer


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        exclude = ["password", "is_admin", "is_staff", "is_superuser"]


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = Account
        fields = ["email", "username", "password", "password2"]
        extra_kwargs = {"password": {"write_only": True}}

    def save(self):
        username = self.validated_data["username"]
        email = self.validated_data["email"]
        password = self.validated_data["password"]
        password2 = self.validated_data["password2"]

        if not username.isalnum():
            raise serializers.ValidationError(
                {"username": "Username should only contain alphanumeric characters"}
            )

        if password != password2:
            raise serializers.ValidationError({"password": "Password does not match"})

        account = Account(username=username, email=email)

        account.set_password(password)
        account.save()
        return account


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=5)
    password = serializers.CharField(max_length=64, min_length=5, write_only=True)
    username = serializers.CharField(max_length=64, min_length=5, read_only=True)
    token = serializers.CharField(max_length=255, min_length=5, read_only=True)

    class Meta:
        model = Account
        fields = ["email", "password", "username", "token"]

    def validate(self, attrs):
        email = attrs.get("email", None)
        password = attrs.get("password", None)

        account = authenticate(email=email, password=password)

        if not account:
            raise AuthenticationFailed("Invalid credentials")

        try:
            token = Token.objects.get(user=account)
        except Token.DoesNotExist:
            token = Token.objects.create(user=account)

        is_expired = is_token_expired(token)

        if is_expired:
            token.delete()
            raise AuthenticationFailed(
                "The Token is expired, login to refresh the token"
            )

        return {
            "id": account.pk,
            "email": account.email,
            "username": account.username,
            "token": token.key,
            "expires_in": formated_expires_in(token),
            "detail": "Successfully authenticated",
        }


class ProfileSerializer(serializers.ModelSerializer):
    posts = PostRelationSerializer(many=True, read_only=True)

    class Meta:
        model = Account
        fields = ["id", "email", "username", "name", "profile_pic", "posts"]


class ProfileUpdateSerializer(serializers.ModelSerializer):
    profile_pic = serializers.ImageField(validators=[image_validator], allow_null=True)

    class Meta:
        model = Account
        fields = ["id", "name", "profile_pic"]
