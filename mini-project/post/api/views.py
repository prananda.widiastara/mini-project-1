from post.models import Post
from post.api.serializers import PostSerializer

from rest_framework import generics
from account.renderers import MyRenderer
from account.authentication import ExpiringTokenAuthentication
from post.permission import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.pagination import PageNumberPagination


class PostList(generics.ListCreateAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = PostSerializer
    pagination_class = PageNumberPagination
    renderer_classes = [MyRenderer]
    queryset = Post.objects.all().order_by("id")

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [ExpiringTokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    serializer_class = PostSerializer
    renderer_classes = [MyRenderer]
    queryset = Post.objects.all()
    lookup_field = "id"

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)
