from rest_framework import serializers
from post.models import Post
from mysite.serializers import CommentRelationSerializer, AccountRelationSerializer


class PostSerializer(serializers.ModelSerializer):
    owner = AccountRelationSerializer(read_only=True)
    comments = CommentRelationSerializer(
        source="comment_set", many=True, read_only=True
    )

    class Meta:
        model = Post
        fields = [
            "id",
            "title",
            "body",
            "created_at",
            "updated_at",
            "owner",
            "comments",
        ]
