# Changelog
All notable changes to this project will be documented in this file.

## [[1.0.0]](https://gitlab.com/prananda.widiastara/mini-project-1/-/tree/v1.0.0) - 2022-03-11

### Added

#### API User endpoint

- User Registration API:
    - Check username and email:
        - if username/email is already in use, return error
    - Confirmation password
        - if password and confirmation password doesn't match, return error

- Auth API:
    - Token auth
    - Get access token

- User Profile API:
    - Get all users
    - RUD Profile (Delete for deleting the user)
    - Profile Picture validation:
        - max file upload is 100 KB
        - picture ratio is 1:1
    - Object permission/authorization for Update & Delete

#### API post endpoint

- Message API:
    - Get all messages
    - CRUD Message
    - Object permission/authorization for Update & Delete

#### API comment endpoint

- Comment API:
    - Get all comments
    - CRUD Comment
    - Object permission/authorization for Update & Delete
