# Configuration Mini Project 1 [`./config/.env`](./config/.env)
The default value here is the value used if no value provided on environment variables (or `.env` file)

## Application Configuration
General app configuration

| Variable | Type | Default | Description |
| ----------------------- | ----- | ------------- | ------------------------- |
| DJANGO_ENV | str | development | App environment [development, staging, production, ...] |
| SECRET_KEY | str |  | Secret Key for making hashes |
| PAGINATION_PAGE_SIZE | int | 10 | Default page size for pagination result |
| TOKEN_EXPIRED_AFTER_SECONDS | int | 3600 | Token expiration time for user auth |

## Media Storage Configuration
Used for cloudinary, media cloud storage

| Variable | Type | Default | Description |
| ----------------------- | ----- | ------------- | ------------------------- |
| CLOUD_NAME | str | einzuji | Cloudinary cloud name |
| API_KEY | str |  | Key for accessing Cloudinary storage |
| API_SECRET | str |  | Secret Key for accessing Cloudinary storage |

## Database Configuration
Used Heroku PostgreSQL for staging & production

| Variable | Type | Default | Description |
| ----------------------- | ----- | ------------- | ------------------------- |
| DATABASE_URL | str |  | Heroku PostgreSQL Database URL |
